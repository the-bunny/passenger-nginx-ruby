#!/bin/sh

# adduser --disabled-password thebunny
# chown -R thebunny /app
chmod 0755 /usr/local/bundle

ln -sf /dev/stdout /app/log/development.log
ln -sf /dev/stdout /opt/nginx/logs/access.log
ln -sf /dev/stderr /opt/nginx/logs/error.log

/opt/nginx/sbin/nginx -g "daemon off;"
