ARG RUBY_VERSION=3.2.1

FROM ruby:${RUBY_VERSION}-alpine AS build
ARG RAILS_VERSION=7.0.4.2
ARG PASSENGER_VERSION=6.0.17
ARG BLD_PKGS="build-base curl-dev pcre-dev openssl-dev wget"
RUN apk add --update --no-cache ${BLD_PKGS} && \
    gem install passenger -v ${PASSENGER_VERSION} && \
    gem install rails -v ${RAILS_VERSION} && \
    passenger-install-nginx-module --auto

#################### Build Stage Complete ####################

FROM ruby:${RUBY_VERSION}-alpine AS final
ARG APP_PATH=/app
WORKDIR ${APP_PATH}
ARG SYS_PKGS="curl pcre openssl wget"
RUN apk add --update --no-cache ${SYS_PKGS} && \
    adduser --disabled-password thebunny && \
    chown -R thebunny ${APP_PATH}
COPY --from=build /usr/local/bundle /usr/local/bundle
COPY --from=build /opt/nginx /opt/nginx
COPY conf/nginx.conf /opt/nginx/conf/nginx.conf
COPY scripts/init.sh /init.sh
CMD [ "/init.sh" ]
