# :whale: [Passenger-Nginx-Ruby](https://hub.docker.com/r/thebunny/passenger-nginx-ruby)

Minimal docker base image using passenger, nginx, & ruby on alpine.

Warning: This image was not built with TLS/SSL in mind as it is intended to be used behind a load balancer service.

### :1234: Current Versions

Image is preloaded with the following libraries:

| Library | Version |
| --- | --- |
| Passenger | 6.0.17 |
| Rails | 7.0.4.2 |
| Ruby | 3.2.1 |

### :cop: Rules

Your rails application root must live at `/app`, and the directory must be owned by `thebunny` user.

The working directory is already set to `/app`, so this is as simple as copying your application files and re-applying permissions:

```dockerfile
COPY . .
...
RUN chown -R thebunny /app
```

### :clapper: Example Usage

Example Dockerfile using this base image to build a standard API-only app:

```dockerfile
ARG RUBY_VERSION=3.2.1

FROM thebunny/passenger-nginx-ruby:${RUBY_VERSION}-alpine AS build-base

ARG BLD_PKGS="build-base gcc make musl-dev"
ARG DEV_PKGS="libpq-dev"
ARG SYS_PKGS="tzdata"

COPY Gemfile* ./
RUN apk add --update --no-cache ${BLD_PKGS} ${DEV_PKGS} ${SYS_PKGS} && \
    bundle config set --local without 'development test' && \
    bundle config --global frozen 1 && \
    bundle check || bundle install

#################### Build Stage Complete ####################

FROM thebunny/passenger-nginx-ruby:${RUBY_VERSION}-alpine AS production

ARG APP_PKGS="libpq"
ARG SYS_PKGS="musl tzdata"

COPY --from=build-base /usr/local/bundle /usr/local/bundle
COPY . .

RUN apk add --update --no-cache ${APP_PKGS} ${SYS_PKGS} && \
    chown -R thebunny /app

################# Production Build Complete ##################

FROM production AS test

RUN bundle config set --local without 'development' && \
    bundle check || bundle install

#################### Test Build Complete #####################

FROM production AS development

RUN bundle config set --local without '' && \
    bundle check || bundle install && \
    sed -i 's/production/development/g' /opt/nginx/conf/nginx.conf

################# Development Build Complete #################
```
